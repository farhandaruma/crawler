
# -*- coding: utf-8 -*-
import scrapy
import csv
from urllib.parse import urlparse, urljoin
from .postgresql import *
from scrapy import signals
from datetime import datetime
from scrapy.spidermiddlewares.httperror import HttpError
from twisted.internet.error import DNSLookupError
from twisted.internet.error import TimeoutError
from selenium import webdriver
import os


def delete_files_in_jobdir(mydir):
    filelist = [f for f in os.listdir(mydir)]
    for f in filelist:
        os.remove(os.path.join(mydir, f))

class BrandListSpider(scrapy.Spider):
    name = 'brand_list'

    custom_settings = {
        'BOT_NAME': 'brandlist_bot',
        'DOWNLOADER_MIDDLEWARES': {
            'scrapy_crawlera.CrawleraMiddleware': 610
        },
        'AUTOTHROTTLE_ENABLED' : True,
        'CRAWLERA_PRESERVE_DELAY' : True,
        'DEPTH_PRIORITY': 1,
        'SCHEDULER_DISK_QUEUE': 'scrapy.squeue.PickleFifoDiskQueue',
        'SCHEDULER_MEMORY_QUEUE': 'scrapy.squeue.FifoMemoryQueue'
    }

    crawlera_enabled = True
    crawlera_apikey = "94e8812c737a4402b857c01033c8426b"

    # scrapy event handler
    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        spider = super(BrandListSpider, cls).from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.request_scheduled, signal=signals.request_scheduled)
        crawler.signals.connect(spider.response_received, signal=signals.response_received)
        crawler.signals.connect(spider.item_scraped, signal=signals.item_scraped)
        crawler.signals.connect(spider.spider_error, signal=signals.spider_error)
        crawler.signals.connect(spider.spider_opened, signal=signals.spider_opened)
        crawler.signals.connect(spider.spider_closed, signal=signals.spider_closed)
        return spider

    def spider_closed(self, spider):
        spider.logger.info('ENGINE %s ENDED' % spider.name)
        job_id = getattr(self, 'job_id', None)
        update_endtime_job(job_id=job_id, end_time=datetime.now())
        jobdir = getattr(self, 'jobdir', None)
        # delete_files_in_jobdir(jobdir)
        # status = get_status(job_id=job_id)
        # if status == 'on process':
        #     update_status_job(job_id=job_id, status="error")
        #     error = "%s~ %s" % ("no item scraped", "check your spider")
        #     update_request_for_exception(job_id=job_id, exception_content=error)
        update_status_job(job_id=job_id, status="done")

    def spider_opened(self, spider):
        spider.logger.info('ENGINE %s STARTED' % spider.name)
        job_id = getattr(self, 'job_id', None)
        update_starttime_job(job_id=job_id, start_time=datetime.now())
        # update_status_job(job_id=job_id, status="on process")

    def request_scheduled(self, request, spider):
        job_id = getattr(self, 'job_id', None)
        spider.logger.info('******** REQUEST URL ********: %s' % request.url)
        insert_request(job_id=job_id, url=request.url, http_status_code=request.meta.get('depth', None),
                       content_type=request.headers.get('Referer', None).decode("utf-8") if request.headers.get(
                           'Referer', None) is not None else None)

    def response_received(self, response, spider):
        spider.logger.info('<<<<<<<<< RESPONSE URL >>>>>>>>: %s' % response.url)
        content_type = response.headers.get("Content-Type", None).decode("utf-8") if response.headers.get(
            "Content-Type", None) is not None else "-"
        update_request(url=response.url, content_type=content_type, status_code=response.status)

    def item_scraped(self, item, response, spider):
        spider.logger.info('------------ INSERTING ITEM DATA ------------')
        daruma_code = get_detail(self.job_id).get("daruma_code", None)
        job_id = get_detail(self.job_id).get("job_id", None)
        insert_item(url=response.url, job_id=job_id, data=str(item), daruma_code=daruma_code,
                    spider="ProductListSpider")
        job = get_job_by_id(job_id=job_id)
        requests = job.get('requests')
        request_done = job.get('requests_200')
        if request_done == requests:
            update_status_job(job_id=job_id, status="done")
        spider.logger.info('------------ ITEM SCRAPED ------------: %s' % response.url)

    def spider_error(self, failure, spider, response):
        spider.logger.info('!!!!!!!!!!!! SPIDER ERROR !!!!!!!!!!!!')
        job_id = getattr(self, 'job_id', None)
        spider.logger.info(str(failure))
        error = "%s~ %s" % (str(failure.getErrorMessage()), str(failure.frames))
        update_request_for_exception(url=response.url, exception_content=error)
        insert_error_count(job_id=job_id)

    def parse_httpbin(self, response):
        self.logger.error("Got successful response from %s" % response.url)

    def errback_httpbin(self, failure):
        self.logger.error(repr(failure))
        if failure.check(HttpError):
            request = failure.request
            self.logger.error('DNSLookupError on %s' % request.url)

        elif failure.check(TimeoutError):
            request = failure.request
            self.logger.error("Timeout error on %s" % request.url)

    def start_requests(self):

        index_url = getattr(self, 'index_url', None)

        if 'perkakasku.com' in index_url:
            yield scrapy.Request(index_url, self.parse_perkakasku_category_page)
        elif 'monotaro.id' in index_url:
            yield scrapy.Request(index_url, self.parse_monotaro_category_page)
        elif 'klikmro.com' in index_url:
            yield scrapy.Request(index_url, self.parse_klikmro_category_page)

        if index_url is not None:
            yield scrapy.Request(index_url, self.parse_index)

    def parse_index(self, response):
        domain = urlparse(response.url).netloc
        if domain.startswith("www."):
            domain = domain[4:]
        
        if domain.lower() == "monotaro.id":
            return self.parse_monotaro_index(response)
        elif domain.lower() == "perkakasku.com":
            return self.parse_perkakasku_index(response)
        elif domain.lower() == "klikmro.com":
            return self.parse_klikmro_index(response)

    def parse_monotaro_index(self, response):
        for cat in response.css(".em-catalog-navigation > .level0"):
            # Main menu
            cat_url = cat.css("a::attr(href)").extract_first()
            title = cat.css("span::text").extract_first()

            yield scrapy.Request(cat_url, self.parse_category_page)

    def parse_perkakasku_index(self, response):        

        for cat in response.css(".category-wrapper .category-menu > li"):
            # Main menu
            cat_url = cat.css("a::attr(href)").extract_first()
            if cat_url and cat_url.startswith("etalase"):
                if not cat_url.startswith("http"):
                    cat_url = urljoin(response.url, cat_url)
                yield scrapy.Request(cat_url, self.parse_category_page)

    def parse_klikmro_index(self, response):
        for cat in response.css(".category-menu .menu-wrapper > ul > li"):
            # Main menu
            cat_url = cat.css("a::attr(href)").extract_first()
            title = cat.css("a > span.cat-name::text").extract_first()

            yield scrapy.Request(cat_url, self.parse_category_page)

    def parse_category_page(self, response):
        domain = urlparse(response.url).netloc
        if domain.startswith("www."):
            domain = domain[4:]

        if domain.lower() == "monotaro.id":
            return self.parse_monotaro_category_page(response)
        elif domain.lower() == "perkakasku.com":
            return self.parse_perkakasku_category_page(response)
        elif domain.lower() == "klikmro.com":
            return self.parse_klikmro_category_page(response)
        else:
            print("Invalid domain:", domain)

    def parse_monotaro_category_page(self, response):

        cat_level = response.meta.get("cat_level", 0)
        parent_cat_name = response.meta.get("parent_cat_name")
        cat_name = response.css(".category-view h1::text").extract_first()
        if parent_cat_name:
            cat_name = parent_cat_name + "//" + cat_name
        cat_url = response.url
        seller = "Monotaro"

        brand_list = None
        subcat_list = None

        section_list = response.css("#narrow-by-list > *")
        for idx, e in enumerate(section_list):
            section_name = e.css("dt .title::text").extract_first()
            if section_name:
                if section_name.lower() == "kategori":
                    subcat_list = section_list[idx+1]
                elif section_name.lower() == "brand":
                    brand_list = section_list[idx+1]

        if brand_list:
            for b in brand_list.css("li"):
                brand_name = b.css("::attr(data-text)").extract_first().strip()
                product_count = b.css(".count::text").extract_first().strip()
                product_count = product_count.replace("(", "").replace(")", "")

                yield {
                    "seller": seller,
                    "category": cat_name,
                    "brand": brand_name,
                    "product_count": product_count
                }

        if subcat_list:
            for c in subcat_list.css("li"):
                subcat_url = c.css("a::attr(href)").extract_first().strip()

                yield scrapy.Request(subcat_url, self.parse_category_page, meta={ "parent_cat_name": cat_name, "cat_level": cat_level+1})

    def parse_perkakasku_category_page(self, response):

        cat_level = response.meta.get("cat_level", 0)
        parent_cat_name = response.meta.get("parent_cat_name")
        cat_name = response.css("h1.title-page::text").extract_first()
        if parent_cat_name:
            cat_name = parent_cat_name + "//" + cat_name
        cat_url = response.url

        brand_list = response.css(".merk optgroup")[0].css("option")

        if brand_list:
            for b in brand_list:
                tag = b.css("::attr(value)").extract_first().strip()
                tag = tag.split("|")[1]
                brand_name = b.css("::text").extract_first().strip()

                i = cat_url.rfind("-")
                brand_url = "{}-{}-{}".format(cat_url[:i].replace("etalase-", "jual-"), tag, cat_url[i+1:])

                yield scrapy.Request(brand_url, self.parse_perkakasku_category_brand_page, 
                    meta={ "cat_name": cat_name, "brand_name": brand_name })

    def parse_perkakasku_category_brand_page(self, response):

        cat_name = response.meta.get("cat_name")
        brand_name = response.meta.get("brand_name")
        seller = "Perkakasku"
        product_count = response.css(".product-breadcrumb mark strong::text").extract_first().strip()

        yield {
            "seller": seller,
            "category": cat_name,
            "brand": brand_name,
            "product_count": product_count,
            "url": response.url
        }

    def parse_klikmro_category_page(self, response):

        cat_level = response.meta.get("cat_level", 0)
        parent_cat_name = response.meta.get("parent_cat_name")
        cat_name = response.css(".category-title h1::text").extract_first()
        if parent_cat_name:
            cat_name = parent_cat_name + "//" + cat_name
        cat_url = response.url
        seller = "KlikMRO"

        brand_list = response.css("dd.brand li")

        if brand_list:
            for b in brand_list:
                brand_name = b.css("::attr(data-text)")
                if not brand_name:
                    continue
                brand_name = brand_name.extract_first().strip()
                product_count = b.css("::text")[1].extract().strip()                
                product_count = product_count.replace("(", "").replace(")", "")

                yield {
                    "seller": seller,
                    "category": cat_name,
                    "brand": brand_name,
                    "product_count": product_count
                }

        subcat_list = response.css("dd.category li")
        if subcat_list:
            for c in subcat_list:
                subcat_url = c.css("a::attr(href)")
                if not subcat_url:
                    continue
                subcat_url = subcat_url.extract_first().strip()

                yield scrapy.Request(subcat_url, self.parse_category_page, meta={ "parent_cat_name": cat_name, "cat_level": cat_level+1})

