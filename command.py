import uuid
import random
import string
import requests
from elasticsearch import Elasticsearch
from postgresql import insert_auto_brand_name, get_image_url_by_item_url, get_price_from_price_table, insert_calculation_price_to_product, get_price_id_by_url, get_price_by_link_id, remove_price_data_product, insert_price_data_product, get_product, check_count_link_id, delete_rows_by_link_id, update_link_id_price, check_link_id, get_link_id_by_item_url, get_price_by_item_url, insert_product
import csv
import pprint
import ast
import collections
import json
import os

HOST = '172.22.0.3'
# HOST = '46.101.77.153'
PORT =  9200

es_object = Elasticsearch([{
            'host': HOST,
            'port': PORT
        }])

def convert_tsv():
    filename= "input.tsv"
    path = os.path.join(os.getcwd(),'auto-brand-name', filename)
    with open(path, encoding="utf-8") as fd:
        rd = csv.reader(fd, delimiter="\t", quotechar='"')
        result = list()
        for item in rd:
            result.append(tuple(dict(
                regex=item[0],
                brand=item[1],
            ).values()))
        return result

def read_price_csv(filename):
    reader = csv.DictReader(open(filename, encoding='utf-8'))
    result = list()
    for row in reader:
        r = dict(
        price_id=row['price_id'],
        mpn = row['mpn'],
        name = row['name'],
        brand = row['brand'],
        price_before_discount = row['price_before_discount'],
        price = row['price'],
        sku = row['sku'],
        supplier_code = row['supplier_code'],
        description = row['description'],
        item_url = row['item_url'],
        store = row['store'],
        last_update = row['last_update'],
        daruma_code = row['daruma_code'],
        store_url = row['store_url'],
        store_name = ast.literal_eval(row['store_name'])[0] if row['store_name'] is not None else '-',
        link_id = row['link_id'],
        sales = row['sales'],
        views = row['views'],
        reviews = row['reviews'],
        total_sales = row['total_sales'],
        image_url = row['image_url'],
        stock = row['stock']
        )
        r['data_custom'] = str(r)
        result.append(r)
    return result

def auto_brand_name_insert_command():
    input = convert_tsv()
    insert_auto_brand_name(input)

def update_price_product(link_id):
    price_list = get_price_from_price_table(link_id=link_id)
    insert_calculation_price_to_product(link_id=link_id, price_data=price_list)

def convert_aggregate_into_csv():
    toCSV = get_product()
    keys = toCSV[0].keys()
    with open('aggregate.csv', 'w', encoding='utf-8') as output_file:
        dict_writer = csv.DictWriter(output_file, keys)
        dict_writer.writeheader()
        dict_writer.writerows(toCSV)

def delete_cache():
    url = 'http://%s:%s/_cache/clear' % (HOST, PORT)
    response = requests.post(url=url)
    result = response.json()['_shards']['failed']
    if result == 0:
        print('cache deleted..')
    else:
        print('cache not deleted')

def auto_match(data):

    mpn = [i[0] for i in data]
    mpn_counter = collections.Counter(mpn)
    duplicate_mpn = [i for i in mpn_counter.keys() if mpn_counter.get(i) > 1]
    mpn_and_brand = [(d[0], d[1]) for i in duplicate_mpn for d in data if d[0] == i]
    mpn_and_brand_counter = collections.Counter(mpn_and_brand)
    duplicate_mpn_and_brand = [i for i in mpn_and_brand_counter.keys() if mpn_and_brand_counter.get(i) > 1]

    def get_id(i):
        return tuple([d[2] for d in data if i[0] == d[0] and i[1] == d[1]])

    return [get_id(i) for i in duplicate_mpn_and_brand]

def delete_docs():
    url = 'http://%s:%s/price/price/_query' % (HOST, PORT)
    query = {
                "query": {
                    "match_all": {}
                }
            }
    try:
        requests.delete(url=url, data=query)
        print("docs deleted")
    except Exception as e:
        print("error: %s" % e)

def delete_doc_by_es_id(es_id):
    url = 'http://%s:%s/price/price/%s' % (HOST, PORT, es_id)

    try:
        response = requests.delete(url=url).json()
        # print(response)
        success = True if response["_shards"]["failed"] == 0 else False
        if success:
            print("docs %s deleted" % es_id)
        else:
            print("delete %s failed" % es_id)
    except Exception as e:
        print("error: %s" % e)

def del_all_index():
    url = 'http://%s:%s/_all' % (HOST, PORT)

    try:
        response = requests.delete(url=url).json()
        print(response)
    except Exception as e:
        print("error: %s" % e)

def update_all_link_id_price(item_url_list, es_ids):

    def generate_link_id():
        return str(uuid.uuid1()).join(random.choices(string.ascii_uppercase + string.digits, k=5))[:8]

    while True:
        link_id = generate_link_id()
        if check_link_id(link_id=link_id):
            print('regenrating link_id..')
        else:
            for url, id in zip(item_url_list, es_ids):
                update_link_id_price(item_url=url, link_id=link_id)
                update_link_id_elasticsearch(link_id=link_id, id=id)
            create_product(urls=item_url_list, link_id=link_id)
            break

def update_current_link_id(item_url_list, link_id, es_ids):
    for url, id in zip(item_url_list, es_ids):
        update_link_id_price(item_url=url, link_id=link_id)
        update_link_id_elasticsearch(link_id=link_id, id=id)
    update_price_product(link_id=link_id)

def update_all_unlink_price(item_url_list, es_ids):
    link = get_link_id_by_item_url(item_url=item_url_list[0])
    id_list = list()
    for url, id in zip(item_url_list, es_ids):
        update_link_id_price(item_url=url, link_id=None)
        update_link_id_elasticsearch(link_id=None, id=id)
        id_list.append(get_price_id_by_url(item_url=url))
    count = check_count_link_id(link_id=link)
    print("count for link_id %s: %s" % (link, count))
    if count == 0:
        delete_rows_by_link_id(link_id=link)
    else:
        update_price_product(link_id=link)

# get the first value of link_ids by link url list
def get_first_value(link_urls):
    result = []
    for url in link_urls:
        result.append(get_link_id_by_item_url(url))
    try:
        res = next(item for item in result if item is not None)
    except:
        res = None
    return res


def create_product(urls, link_id):
    url = urls[0]
    price = get_price_by_item_url(item_url=url)
    insert_product(link_id=link_id,
                   name=price.get('name', '-'),
                   brand=price.get('brand', '-'),
                   sku=price.get('sku', '-'),
                   supplier_code=price.get('supplier_code', '-'),
                   description=price.get('description', '-'),
                   last_update=price.get('last_update', '-'))
    update_price_product(link_id=link_id)
    print("product was created with link_id: %s" % link_id)

# validate list items
def is_valid(list_items):
    clean = [i for i in list_items if i is not None]
    s = set(clean)
    if len(s) > 1:
        return False
    else:
        return True

def get_links_by_urls(link_urls):
    result = []
    for url in link_urls:
        result.append(get_link_id_by_item_url(url))
    return result

def search(index_name, keyword, frm=0, size=100):

    # delete_cache()
    query = {
        "sort": [
            # {
            #     "name":
            #      {
            #          "order" : "asc",
            #          # "mode" : "max",
            #          "missing" : "_last",
            #      }
            # },
            "_score"
        ],
        "from": frm,
        "size": size,
        'query' : {
            'multi_match' : {
                'fields' : ['name^3', 'link_id', 'description'],
                'type': 'best_fields',
                'query' : keyword,
                'fuzziness' : 'AUTO',
                'tie_breaker': 0.3
            }
        }
    }

    # query = {
    #         "from" : 0, "size" : 30,
    #         "query" : {
    #             "term" : { "name" : keyword }
    #         }
    #     }


    results = es_object.search(index_name, body=query)['hits']['hits']
    # ids = es_object.search(index_name, body=query, filter_path=['hits.hits._id'])['hits']['hits']
    prod = list()
    count_id = 0
    for result in results:
        id = result['_source'].get('id', '-')
        name = result['_source'].get('name', '-')
        brand = result['_source'].get('brand', '-')
        price_before_discount = result['_source'].get('price_before_discount', '-')
        price = result['_source'].get('price', '-')
        sku = result['_source'].get('sku', '-')
        supplier_code = result['_source'].get('supplier_code', '-')
        desc = result['_source'].get('description', '-')
        if desc is not None:
            description = "%s.." % desc[:100] if len(desc) > 40 else desc
        else:
            description = '-'
        item_url = result['_source'].get('item_url', '-')
        store = result['_source'].get('store', '-')
        last_update = result['_source'].get('last_update', '-')
        daruma_code = result['_source'].get('daruma_code', '-')
        rating = result['_source'].get('rating', '-')
        store_url = result['_source'].get('store_url', '-')
        image_url = result['_source'].get('image_url', '-')
        # image_url = get_image_url_by_item_url(item_url=item_url)
        link_id = get_link_id_by_item_url(item_url=item_url)
        sales = result['_source'].get('sales', '-')
        views = result['_source'].get('views', '-')
        reviews = result['_source'].get('reviews', '-')
        total_sales = result['_source'].get('total_sales', '-')
        es_id = result['_id']
        count_id += 1
        d = dict(
            id=id,
            name=name,
            brand=brand,
            price_before_discount=price_before_discount,
            price=price,
            sku=sku,
            supplier_code=supplier_code,
            description=description,
            item_url=item_url,
            store=store,
            last_update=last_update,
            daruma_code=daruma_code,
            rating=rating,
            store_url=store_url,
            image_url=image_url,
            link_id=link_id if link_id is not None else '',
            es_id=es_id,
            full_description=desc,
            sales=sales,
            views=views,
            reviews=reviews,
            total_sales=total_sales,
            count_id="count_id%s" % count_id
        )
        prod.append(d)
    return prod

def get_all_documents():

    # delete_cache()
    query = {
            "query" : {
                "match_all" : {}
            }
        }
    results = es_object.search('price', body=query)['hits']['hits']
    # ids = es_object.search(index_name, body=query, filter_path=['hits.hits._id'])['hits']['hits']
    prod = list()
    for result in results:
        id = result['_source']['id']
        name = result['_source']['name']
        brand = result['_source']['brand']
        price_before_discount = result['_source']['price_before_discount']
        price = result['_source']['price']
        sku = result['_source']['sku']
        supplier_code = result['_source']['supplier_code']
        description = result['_source']['price']
        item_url = result['_source']['item_url']
        store = result['_source']['store']
        last_update = result['_source']['last_update']
        daruma_code = result['_source']['daruma_code']
        rating = result['_source']['rating']
        store_url = result['_source']['store_url']
        image_url = result['_source']['image_url']
        link_id = result['_source']['link_id']
        es_id = result['_id']
        d = dict(
            id=id,
            name=name,
            brand=brand,
            price_before_discount=price_before_discount,
            price=price,
            sku=sku,
            supplier_code=supplier_code,
            description=description,
            item_url=item_url,
            store=store,
            last_update=last_update,
            daruma_code=daruma_code,
            rating=rating,
            store_url=store_url,
            image_url=image_url,
            link_id=link_id if link_id is not None else '',
            es_id=es_id
        )
        prod.append(d)
    return prod

def update_link_id_elasticsearch(link_id, id):
    try:
        es_object.update(index='price', doc_type='price', id=id,
                    body={"doc": {"link_id": link_id}})
        print("link_id for price id %s was updated" % id)
    except Exception as e:
        print("ERROR update link_id elasticsearch: %s" % e)

if __name__ == '__main__':
    pass
