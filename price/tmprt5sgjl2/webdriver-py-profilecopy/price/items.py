# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html
from selenium import webdriver
import os
from scrapy.utils.python import to_bytes

import scrapy


class PriceItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    pass


if __name__ == "__main__":
    url = 'https://www.tokopedia.com/gudangpowertools/page/2'
    options = webdriver.FirefoxOptions()
    options.add_argument('-headless')
    driver = webdriver.Firefox(firefox_options=options, executable_path='%s/geckodriver' % os.getcwd())
    driver.get(url)
    page = driver.page_source
    current_url = driver.current_url
    result = dict(page=page, current_url=current_url)
    driver.quit()
    print('grid-shop-product' in result['page'])